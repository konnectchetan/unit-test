import org.junit.*;


public class TestCalculate {

    @Test
    public void testSum(){
        System.out.println("testSum getting executed");
        //arrange
        Calculate calc =new Calculate();
        int a = 4;
        int b = 5;
        int expected = 9;
        //act
        int actual = calc.sum(a,b);
        //assert
        Assert.assertEquals(expected,actual);
    }
    @Ignore
    public void testDiff(){
        System.out.println("testDiff getting executed");
        //arange
        Calculate calc =new Calculate();
        int a=6;
        int b=5;
        int expected =1;
        //act
        int actual = calc.diff(a,b);
        //assert
        Assert.assertEquals(expected,actual);
    }
    @Before
    public void runBeforeEveryTest(){
        System.out.println("method associated with @before annotation");
    }
    @After
    public void runAfterEveryTest(){
        System.out.println("method associated with @after annotation");
    }
    @BeforeClass
    public static void runBeforeClass(){
        System.out.println("method associated with @BeforeClass annotation");
    }
    @AfterClass
    public static void runAfterClass()
    {
        System.out.println("method associated with @AfterClass annotation");
    }
}
